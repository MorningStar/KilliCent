package app;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.Key;
import java.util.ArrayList;
import java.util.Scanner;

import security.SecUtil;
import connection.Client;
import connection.MessageHandler;
import connection.Server;
import millicent.Scrip;
import millicent.ScripMinter;
import network_data.Message;
import network_data.Message.MessageEncryptedData;
import network_data.MessageCommand;
import network_data.Message.MessageID;

public class Broker implements MessageHandler{
	private static final int brokerScripLimit = 100000; 
//	private static int brockeRegIdRange = 100000;
	
	public static class VendorInfo{
		private int vendorId;
		private String host;
		private int port;
		private Key key;
		private ScripMinter vendorMinter;
		public VendorInfo(int vendorId, String host, int port, Key key){
			super();
			this.vendorId = vendorId;
			this.host = host;
			this.port = port;
			this.key = key;
		}
		
		public void setVendorMinter(ScripMinter vendorMinter) {
			this.vendorMinter = vendorMinter;
		}
		public int getVendorId() {
			return vendorId;
		}
		public String getHost() {
			return host;
		}
		public int getPort() {
			return port;
		}
		public Key getKey() {
			return key;
		}
		public ScripMinter getVendorMinter() {
			return vendorMinter;
		}
		
	}
	private static class VendorMinterRequester extends Thread{
		private VendorInfo vendorInfo;
		private int brokerId;
		
		public VendorMinterRequester(VendorInfo vendorInfo, int brokerId) {
			super();
			this.vendorInfo = vendorInfo;
			this.brokerId = brokerId;
		}
		@Override
		public void run() {
			try{
				Message message = new Message(brokerId, vendorInfo.getVendorId(), 0);
				message.setCommand(new MessageCommand(MessageCommand.REQ_VENDOR_MINT));
				Message result = Client.sendMessageAndGetResult(message, vendorInfo.getHost(), vendorInfo.getPort());
				if(result.getStatus() != 0){
					System.err.println("INVALID MINTER ERROR");
					return;
				}
				vendorInfo.setVendorMinter(result.readMessageEncryptedData(vendorInfo.getKey()).getScripMinter());
			}catch(Exception e){
			}
		}
	}
	private int brokerId;
	private ScripMinter brokerMinter;
	private ArrayList<VendorInfo> vendorInfos;

	private int registerCustomerHolder, registerVendorHolder;
	
	private synchronized int getRegisteredCustomerId(){
		return registerCustomerHolder++;
	}
	private synchronized int getRegisteredVendorId(){
		return registerVendorHolder++;
	}

	public Broker(int brokerId, int registerVendorHolder) {
		super();
		this.brokerId = brokerId;
		this.registerVendorHolder = registerVendorHolder;
		registerCustomerHolder = 1000;
		vendorInfos = new ArrayList<Broker.VendorInfo>();
		brokerMinter = ScripMinter.getInstance(brokerScripLimit);
		brokerMinter.generateConsumedRange();
	}

	private void requestVendorMinter(int vendorId){
		VendorInfo vendorInfo = findVendorInfo(vendorId);
		if(vendorInfo == null){
			showWarningResult("Invalid vendor minter request");
			return;
		}
		new VendorMinterRequester(vendorInfo, brokerId).run();
	}
	
	private void showAcceptedResult(String s){
		System.out.println(s + ".");
	}
	private void showWarningResult(String s){
		System.out.println(s + ".");
	}
	
	private ScripMinter getVendorMinter(int vendorId){
		for(int i = 0; i < vendorInfos.size(); i++)
			if(vendorInfos.get(i).getVendorId() == vendorId)
				return vendorInfos.get(i).getVendorMinter();
		return null;
	}
	private VendorInfo findVendorInfo(int vendorId){
		for(int i = 0; i < vendorInfos.size(); i++)
			if(vendorInfos.get(i).getVendorId() == vendorId)
				return vendorInfos.get(i);
		return null;
	}
	

	private void respondAskId(Socket socket, Message message){
//		System.err.println("RESPOND ASK ID MESSAGE.");
		Message respond = new Message(0, 0, 0);
		ArrayList<MessageID> ids = new ArrayList<Message.MessageID>();
		ids.add(new MessageID(brokerId, -1));
		respond.setRequestedIds(ids);
//		System.err.println("respondAskId1 send response: " + respond);
		Client.sendMessageReply(respond, socket);
//		System.err.println("respondAskId2 send response: " + respond);
	}
	private void respondRegisterClient(Socket socket, Message message){
		Message respond = new Message(0, 0, 0);
		int regId = getRegisteredCustomerId();
		respond.setAskedId(regId);
		respond.setExchangeKey(brokerMinter.getCustumerKey(regId));
		Client.sendMessageReply(respond, socket);
		showAcceptedResult("new client registered with id " + regId);
	}
	private void respondRegisterVendor(Socket socket, Message message){
		Message respond = new Message(0, 0, 0);
		int regId = getRegisteredVendorId();
		Key key = SecUtil.buildSecretKey();
		respond.setAskedId(regId);
		respond.setExchangeKey(key);
		Client.sendMessageReply(respond, socket);
		VendorInfo vendorInfo = new VendorInfo(regId, socket.getInetAddress().getHostAddress(), message.getValue(), key);
		vendorInfos.add(vendorInfo);
		requestVendorMinter(regId);
		showAcceptedResult("new vendor registered with id " + regId);
	}
	private void respondSellBrokerScrip(Socket socket, Message message){
		if(message.getVendorId() != brokerId){
			showWarningResult("Invalid message");
			return;
		}
		Message respond = new Message(message.getVendorId(), message.getCustumerId(), 0);
		MessageEncryptedData encryptedData = MessageEncryptedData.factoryBuilBrokerScrip(brokerMinter.mintScrip(message.getValue(), message.getCustumerId()));
		respond.writeMessageEncryptedData(encryptedData, brokerMinter.getCustumerKey(message.getCustumerId()) );
		Client.sendMessageReply(respond, socket);
		showAcceptedResult("Sold " + message.getValue() + "S broker scrip");
	}
	private void respondSellVendorScrip(Socket socket, Message message){
		if(message.getVendorId() != brokerId){
			showWarningResult("Invalid message");
			return;
		}
		Message respond;
		Key key = brokerMinter.getCustumerKey(message.getCustumerId());
		MessageEncryptedData encryptedData = message.readMessageEncryptedData(key);
		Scrip customerScrip =  encryptedData.getBrokerScrip();
		ScripMinter vendorMinter = getVendorMinter(message.getAskedId());
		
		if(vendorMinter == null || vendorMinter.canMint() == false){
			showWarningResult(message.getAskedId() + " minter expired");
			respond =  new Message(message.getVendorId(), message.getCustumerId(), Message.ERROR_SERVER_IS_DOWN);
			respond.setError("please try again in few minutes");
			Client.sendMessageReply(respond, socket);
			requestVendorMinter(message.getAskedId());
			return;
		}
		int remainingMoney = customerScrip.getScriptData().getValue() - encryptedData.getMessageRequest().getValue();
		if(remainingMoney < 0){
			showWarningResult("insufficent money. send unautherized message");
			respond =  new Message(message.getVendorId(), message.getCustumerId(), Message.ERROR_INVALID_SCRIP);
			respond.setError("insufficent money");
			Client.sendMessageReply(respond, socket);
			return;
		}
		boolean paymentDone = brokerMinter.consumeScripIfValid(customerScrip);
		if(paymentDone == false){
			showWarningResult("Invalid scrip. send unautherized message");
			respond =  new Message(message.getVendorId(), message.getCustumerId(), Message.ERROR_INVALID_SCRIP);
			respond.setError("Invalid payment");
			Client.sendMessageReply(respond, socket);
			return;
		}
		
		respond = new Message(message.getVendorId(), message.getCustumerId(), 0);
		
		Scrip vendorScrip = vendorMinter.mintScrip(encryptedData.getMessageRequest().getValue(), message.getCustumerId());
		Scrip brokerChangeScrip = brokerMinter.mintScrip(remainingMoney, message.getCustumerId());
		MessageEncryptedData respondData = MessageEncryptedData.factoryBuildBrokerVendorExchange(vendorMinter.getCustumerKey(message.getCustumerId()), brokerChangeScrip, vendorScrip, vendorMinter.getMinterId());
		respond.writeMessageEncryptedData(respondData, brokerMinter.getCustumerKey(message.getCustumerId()) );
		Client.sendMessageReply(respond, socket);
		showAcceptedResult("changed " + encryptedData.getMessageRequest().getValue() + "S scrip with " + remainingMoney + "S remainig broker scrip");
	}
	

	public void handleMessage(Socket socket) {
		Message message = null;
		try{
//			System.err.println("socket: " + socket);
//			System.err.println("socketInputStream: " + socket.getInputStream());
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
//			System.err.println("objectreader:" + ois);
			Object obj = ois.readObject();
//			System.err.println("read obj:" + obj);
			if(obj instanceof Message)
				message = (Message)obj;
			else{
				showWarningResult("Invalid message");
			}
			MessageCommand command = message.getCommand();
//			System.err.println("Broker handle message: " + message);
			if(command.getCommand().equals(MessageCommand.ASK_ID)){
				respondAskId(socket, message);
			}
			else if(command.getCommand().equals(MessageCommand.REGISTER_CLIENT)){
				respondRegisterClient(socket, message);
			}
			else if(command.getCommand().equals(MessageCommand.REGISTER_VENDOR)){
				respondRegisterVendor(socket, message);
			}
			else if(command.getCommand().equals(MessageCommand.BUY_BROKER_SCRIP)){
				respondSellBrokerScrip(socket, message);
			}
			else if(command.getCommand().equals(MessageCommand.BUY_VENDOR_SCRIP)){
				respondSellVendorScrip(socket, message);
			}
			else {
				showWarningResult("Invalid message command. message terminated");
			}
		}catch(Exception e){
			showWarningResult("Invalid connection terminated");
			System.err.println("ERROR : " + e.getMessage());
//			e.printStackTrace();
		}finally{
//			System.err.println("FINALY");
			try {
				socket.close();
			} catch (IOException e) {
			}
		}
		
	}
	public void run(){
		Scanner scanner = null;
		try {
			System.out.println("RUNNING MILLICENT BROKER.");
			int port;
			scanner = new Scanner(System.in);
			System.out.println("please enter port number");
			port = scanner.nextInt();
			Server server;
			server = new Server(new ServerSocket(port, 6), this);
			server.start();
		} catch (IOException e) {
			System.out.println("unable to run server.");
			System.out.println("program terminated.");
		}finally{
			scanner.close();
		}
	}
}
