package security;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

public class SecUtil {
	private static String encryptionType = "AES/ECB/PKCS5Padding";
	
	public static final byte[] intToByteArray(int value) {
	    return new byte[] {
	            (byte)(value >>> 24),
	            (byte)(value >>> 16),
	            (byte)(value >>> 8),
	            (byte)value};
	}
	public static byte[] appendByteArrays(final byte[] a,final  byte[] b){
		byte[] c = new byte[a.length+b.length];
		System.arraycopy(a, 0, c, 0, a.length);
		System.arraycopy(b, 0, c, a.length, b.length);
		return c;
	}
	
	public static String byteArrayToHex(final byte[] a) {
		   StringBuilder sb = new StringBuilder(a.length * 2);
		   for(byte b: a)
		      sb.append(String.format("%02x", b & 0xff));
		   return sb.toString();
		}
	public static boolean isByteArraysEqual(final byte[] a, final byte[] b){
		if(a.length != b.length)
			return false;
		for(int i = 0; i < a.length; i++)
			if(a[i] != b[i])
				return false;
		return true;
	}
	public static Key buildSecretKey(){
		Key key = null;
    	try {
        	KeyGenerator generator;
    		generator = KeyGenerator.getInstance("AES");
    		generator.init(128);
	    	key = generator.generateKey();
		} catch (NoSuchAlgorithmException e) {
			System.err.println("INVALID KEY");
			e.printStackTrace();
			System.exit(0);
		}
    	return key;
	}
	public static Key buildSecretKeyByMD5(final byte[] data){
		return new SecretKeySpec(Arrays.copyOf(generateMD5Hash(data),16),  "AES");
	}
	
	private static void InitCipher(Cipher cipher, boolean isEnc, Key key) throws Exception{
		int isCipherEnc = Cipher.DECRYPT_MODE;
		if(isEnc)
			isCipherEnc = Cipher.ENCRYPT_MODE;
		cipher.init(isCipherEnc, key);
	}
	
	public static byte[] crypt(boolean isEnc, Key key, final byte[] data){
//		System.err.println((isEnc ? "en" : "de") + "crypt with key :" + key);
		byte[] cryptData = null, output = null;
		
		try{
		    Cipher cipher = Cipher.getInstance(encryptionType);
		    InitCipher(cipher, isEnc, key);
		    cryptData = new byte[cipher.getOutputSize(data.length)];
		    int ctLength =  cipher.update(data, 0, data.length, cryptData, 0);
		    ctLength += cipher.doFinal(cryptData, ctLength);
		    output = Arrays.copyOf(cryptData, ctLength);
//		    System.err.println("crypted data CTLenght: " + output.length + " Data:" + output);
		}catch(Exception e){
			System.err.println("ERROR: CRYPTION");
	    	e.printStackTrace();
		}
		return output;
	}
	
	public static byte[] generateMD5Hash(final byte[] data){
		if(data == null)
			return null;
		MessageDigest messageDigest = null;
		try {
			messageDigest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			System.err.println("ERROR: MD5");
			e.printStackTrace();
		}
		return messageDigest.digest(data);
	}
	
	public static byte[] convertObjectToByteArray(Serializable obj){
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos;
		byte[] out = null;
		try {
			oos = new ObjectOutputStream(bos);
			oos.writeObject(obj);
			out = bos.toByteArray();
			oos.close();
			
		} catch (IOException e) {
			System.err.println("ERROR: CONVERT TO BYTE");
			e.printStackTrace();
		}
		
		return out;
	}
	public static Object convertByteArrayToObject(final byte[] data){
		ByteArrayInputStream bis = new ByteArrayInputStream(data);
		ObjectInputStream ois;
		Object obj = null;
		try {
			ois = new ObjectInputStream(bis);
			obj = ois.readObject();
			ois.close();
			bis.close();
		} catch (Exception e) {
			System.err.println("ERROR: CONVERT TO OBJECT");
			e.printStackTrace();
		}
		return obj;
		
	}

}
