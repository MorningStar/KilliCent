package network_data;

import java.io.Serializable;
import java.security.Key;
import java.util.ArrayList;

import security.SecUtil;
import millicent.Scrip;
import millicent.ScripMinter;

public class Message implements Serializable{
	private static final long serialVersionUID = -5131013682080458348L;
	public static final int ERROR_SOKET_TIMEOUT_EXP = 1;
	public static final int ERROR_INVALID_SCRIP = 2;
	public static final int ERROR_SERVER_IS_DOWN = 3;
	public static class MessageEncryptedData implements Serializable{
		private static final long serialVersionUID = 671511943174219682L;
		private Key encryptedKey;
		private Scrip brokerScrip;
		private Scrip vendorScrip;
		private MessageRequest messageRequest;
		private ScripMinter scripMinter;
		private int mintId;
		
		private MessageEncryptedData(Key encryptedKey, Scrip brokerScrip,
				Scrip vendorScrip, MessageRequest messageRequest, ScripMinter scripMinter, int mintId) {
			super();
			this.encryptedKey = encryptedKey;
			this.brokerScrip = brokerScrip;
			this.vendorScrip = vendorScrip;
			this.messageRequest = messageRequest;
			this.scripMinter = scripMinter;
			this.mintId = mintId;
		}
		
		public static MessageEncryptedData factoryBuilBrokerScrip(Scrip brokerScrip){
			return new MessageEncryptedData(null, brokerScrip, null, null, null, -1);
		}
		public static MessageEncryptedData factoryBuildBuyVendorScrip(Scrip brokerScrip, MessageRequest vendorIdRequest){
			return new MessageEncryptedData(null, brokerScrip, null, vendorIdRequest, null, -1);
		}
		public static MessageEncryptedData factoryBuildBrokerVendorExchange(Key key, Scrip brokerScrip, Scrip vendorScrip, int mintId){
			return new MessageEncryptedData(key, brokerScrip, vendorScrip, null, null, mintId);
		}
		public static MessageEncryptedData factoryBuildBuyFromVendor(Scrip vendorScrip, MessageRequest request, int mintId){
			return new MessageEncryptedData(null, null, vendorScrip, request, null, mintId);
		}
		public static MessageEncryptedData factoryBuildVendorSell(Key key,Scrip vendorScrip, MessageRequest request, int mintId){
			return new MessageEncryptedData(key, null, vendorScrip, request, null, mintId);
		}
		public static MessageEncryptedData factoryBuilRequestScripMinter(ScripMinter minter){
			return new MessageEncryptedData(null, null, null, null, minter, -1);
		}

		
		public int getMintId() {
			return mintId;
		}
		public Key getEncryptedKey() {
			return encryptedKey;
		}
		public Scrip getBrokerScrip() {
			return brokerScrip;
		}
		public Scrip getVendorScrip() {
			return vendorScrip;
		}
		public MessageRequest getMessageRequest() {
			return messageRequest;
		}
		public ScripMinter getScripMinter() {
			return scripMinter;
		}
	}
	public static class MessageID implements Serializable{
		private static final long serialVersionUID = 7632221717474232494L;
		private int brokerId, vendorId;
		public MessageID(int brokerId, int vendorId) {
			super();
			this.brokerId = brokerId;
			this.vendorId = vendorId;
		}
		public int getBrokerId() {
			return brokerId;
		}
		public int getVendorId() {
			return vendorId;
		}
	}
	
	private int vendorId, custumerId, mintId;
	private int status;
	private String error;
	
	private MessageCommand command;
	private byte[] messageEncryptedDatabytes;
	private int askedId, value;
	private ArrayList<MessageID> requestedIds;
	private Key exchangeKey;
	
	public Message(int vendorId, int custumerId, int status) {
		super();
		this.vendorId = vendorId;
		this.custumerId = custumerId;
		this.status = status;
		
		mintId = -1;
		error = null;
		command = null;
		messageEncryptedDatabytes = null;
		askedId = -1;
		exchangeKey = null;
		requestedIds = null;
	}

	
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public int getMintId() {
		return mintId;
	}
	public void setMintId(int mintId) {
		this.mintId = mintId;
	}
	public ArrayList<MessageID> getRequestedIds() {
		return requestedIds;
	}
	public void setRequestedIds(ArrayList<MessageID> requestedIds) {
		this.requestedIds = requestedIds;
	}
	public void setAskedId(int askedId) {
		this.askedId = askedId;
	}
	public int getAskedId() {
		return askedId;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public MessageCommand getCommand() {
		return command;
	}
	public void setCommand(MessageCommand command) {
		this.command = command;
	}
	public Key getExchangeKey() {
		return exchangeKey;
	}
	public void setExchangeKey(Key exchangeKey) {
		this.exchangeKey = exchangeKey;
	}
	public int getVendorId() {
		return vendorId;
	}
	public int getCustumerId() {
		return custumerId;
	}
	public int getStatus() {
		return status;
	}
	
	public void writeMessageEncryptedData(MessageEncryptedData data, Key key){
		messageEncryptedDatabytes =  SecUtil.crypt(true, key, SecUtil.convertObjectToByteArray(data));
	}
	public MessageEncryptedData readMessageEncryptedData(Key key){
		if(messageEncryptedDatabytes == null)
			return null;
		Object obj = SecUtil.convertByteArrayToObject(SecUtil.crypt(false, key, messageEncryptedDatabytes));
		if(obj instanceof MessageEncryptedData)
			return (MessageEncryptedData)obj ;
		return null;
	}

	@Override
	public String toString() {
		return  "Message[command:" + command + " , vId:" + vendorId + " , cId:" + custumerId + " , askId:" + askedId + " , value:" + value + " , status:" + status + " , mintId:" + mintId + " , error:" + error + "]"; 
	}
}
