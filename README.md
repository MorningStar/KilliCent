# KilliCent
KilliCent is a micropayment system based on MilliCent protocol. KilliCent can handle transactions from 0.001$ to 5$ through scrips. Millicent protocol is lightweight and like many other micropayments use its small limit for scrips as a security measure. In this protocol, the computational cost of forging a scrips is more than its value.  

# Users
## Broker
Customers can buy scrips from brokers with macro payments. Brokers allow the customer to exchange their broker scrips with specific vendor scrips, which allows the customer to spend any amount of money on any of supported vendors. 

## Vendors
Vendors sell specific service to clients. The cost of this service is small, which makes macro payments likes PayPal or bank transaction ineffective. Vendors register at a broker and let it mint scrip for them. The broker sells vendors scrip to customers and periodically pays the vendor for these scrips through macro payment.

## Customer
Customers want to get services with a small value from some vendors. They register and pay to the broker and it handles their payments to vendors.
