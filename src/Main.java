import java.util.Scanner;

import app.Broker;
import app.Customer;
import app.Vendor;

public class Main {
//	private void secUtilTest(){
//		String[] s = {"salam", "Hello", "Hi", "Hi"};
//		HashSet<String> e = new HashSet<String>();
//		e.add(s[1]);
//		e.add(s[2]);
//		
//		byte[] bA[] = {s[0].getBytes("UTF-8"), s[1].getBytes("UTF-8"), s[2].getBytes("UTF-8"), s[3].getBytes("UTF-8")}; 
//		
//		for(int i = 0; i < 4; i++){
//			System.err.println(s[i] + " #MD5: " + SecUtil.byteArrayToHex(SecUtil.generateMD5Hash(bA[i])) + " @MD5Str:" + SecUtil.generateMD5Hash(bA[i]) +" #Key:" +  SecUtil.buildSecretKeyByMD5(bA[i]));
//		}
//		System.err.println("EQ:2,3 : " + SecUtil.isByteArraysEqual(SecUtil.generateMD5Hash(bA[2]), SecUtil.generateMD5Hash(bA[3])) );
//		String plainTex = "this is plain text. longer plain text";
//		byte[] plain = plainTex.getBytes("UTF-8"), cipher, decipher;
//		Key key = SecUtil.buildSecretKeyByMD5(bA[1]);
//		cipher = SecUtil.crypt(true, key, plain);
//		System.err.println("cipher:" + SecUtil.byteArrayToHex(cipher) + " $=" + new String(cipher));
//		decipher = SecUtil.crypt(false, key, cipher);
//		System.err.println("decipher:" + SecUtil.byteArrayToHex(decipher)+ " $=" + new String(decipher));
//		
//		byte[] setBytes = SecUtil.convertObjectToByteArray(e);
//		System.err.println("set to #" + setBytes.length + ":" + SecUtil.byteArrayToHex(setBytes));
//		HashSet<String> e2 = (HashSet<String>)SecUtil.convertByteArrayToObject(setBytes);
//		System.err.println("e2:" + e2);
//	}
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		System.out.println("chose your rule:");
		System.out.println("    [1] : customer");
		System.out.println("    [2] : broker");
		System.out.println("    [3] : vendor");
		int type = scanner.nextInt();
		if(type == 1)
			new Customer().run();
		else if(type == 2){
			int vendorIdRange, brokerId;
			System.out.println("Please enter broker id");
			brokerId = scanner.nextInt();
			System.out.println("Please enter vendor id range");
			vendorIdRange = scanner.nextInt();
			new Broker(brokerId, vendorIdRange).run();
		}
		else if(type == 3)
			new Vendor().run();
		else
			System.out.println("Invalid type program terminated.");
		scanner.close();
	}

}
