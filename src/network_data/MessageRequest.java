package network_data;

import java.io.Serializable;

public class MessageRequest implements Serializable{
	private static final long serialVersionUID = 7890568038148214680L;
	private String description;
	private int value;
	
	public MessageRequest(String description, int value) {
		super();
		this.description = description;
		this.value = value;
	}
	
	public String getDescription() {
		return description;
	}
	public int getValue() {
		return value;
	}
}
