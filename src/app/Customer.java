package app;

import java.security.Key;
import java.util.ArrayList;
import java.util.Scanner;

import app.Customer.VendorKey.ScripKey;
import connection.Client;
import network_data.Message;
import network_data.Message.MessageEncryptedData;
import network_data.Message.MessageID;
import network_data.MessageCommand;
import network_data.MessageRequest;
import millicent.Scrip;

public class Customer {
	public static class BrokerKey{
		private int brokerId, customerId, port;
		private Key key;
		private String host;
		private ArrayList<Scrip> scrips;
		public BrokerKey(int brokerId,  String host, int port) {
			super();
			this.brokerId = brokerId;
			this.customerId = -1;
			this.key = null;
			this.host = host;
			this.port = port;
			this.scrips = new ArrayList<Scrip>();
		}

		
		public ArrayList<Scrip> getScrips() {
			return scrips;
		}
		public void addScrip(Scrip scrip){
			if(scrip == null)
				return;
			scrips.add(scrip);
		}
		public void removeScrip(Scrip scrip){
			scrips.remove(scrip);
		}
		public Scrip getScripWithValue(int val){
			if(scrips == null)
				return null;
			for(int i = 0; i < scrips.size(); i++)
				if(scrips.get(i).getScriptData().getValue() >= val)
					return scrips.get(i);
			return null;
		}
		public void setRegisteredInfo(int customerId, Key key) {
			this.customerId = customerId;
			this.key = key;
		}
		public int getCustomerId() {
			return customerId;
		}
		public Key getKey() {
			return key;
		}
		public int getBrokerId() {
			return brokerId;
		}
		public int getPort() {
			return port;
		}
		public String getHost() {
			return host;
		}
	}
	public static class VendorKey{
		public static class ScripKey{
			private Scrip scrip;
			private Key key;
			private int mintId;
			
			public ScripKey(Scrip scrip, Key key, int mintId) {
				super();
				this.scrip = scrip;
				this.key = key;
				this.mintId = mintId;
			}
			public int getMintId() {
				return mintId;
			}
			public Scrip getScrip() {
				return scrip;
			}
			public Key getKey() {
				return key;
			}
		}
		private int brokerId, vendorId;
		private ArrayList<ScripKey> scrips;
		private String host;
		private int port;
		public VendorKey(int brokerId, int vendorId, String host, int port) {
			super();
			this.brokerId = brokerId;
			this.vendorId = vendorId;
			this.host = host;
			this.port = port;
			scrips = new ArrayList<Customer.VendorKey.ScripKey>();
		}
		
		public ArrayList<ScripKey> getScrips() {
			return scrips;
		}
		public int getBrokerId() {
			return brokerId;
		}
		public int getVendorId() {
			return vendorId;
		}
		public String getHost() {
			return host;
		}
		public int getPort() {
			return port;
		}
		public void addScrip(ScripKey scripKey){
			if(scripKey == null || scripKey.getScrip() == null)
				return;
			scrips.add(scripKey);
		}
		public void removeScrip(ScripKey scripKey){
			scrips.remove(scripKey);
		}
		public ScripKey getScripKeyWithValue(int val){
			if(scrips == null)
				return null;
			for(int i = 0; i < scrips.size(); i++)
				if(scrips.get(i).getScrip().getScriptData().getValue() >= val)
					return scrips.get(i);
			return null;
		}
	}
	
	private ArrayList<BrokerKey> brokerKeys;
	private ArrayList<VendorKey> vendorKeys;
	
	private BrokerKey getBrokerKey(int brokerId){
		for(int i = 0; i < brokerKeys.size(); i++)
			if(brokerKeys.get(i).getBrokerId() == brokerId)
				return brokerKeys.get(i);
		return null;
	}
	private VendorKey getVendorKey(int brokerId, int vendorId){
		for(int i = 0; i < vendorKeys.size(); i++)
			if(vendorKeys.get(i).getBrokerId() == brokerId && vendorKeys.get(i).getVendorId() == vendorId)
				return vendorKeys.get(i);
		return null;
	}
	
	private void messageErrorHandler(Message error){
		if(error.getError() != null)
			System.out.println("ERROR: " + error.getError() + ".");
		else
			System.out.println("UNKNOWN ERROR: INSTRUCTION TERMINATED.");
	}
	private void showAcceptedResult(String s){
		System.out.println(s + ".");
	}
	private void showWarningResult(String s){
		System.out.println(s + ".");
	}
	private void showUnknownBrokerError(int brokerId){
		showWarningResult("Broker " + brokerId +  " is unknown. you should ask its ID from <host>:<port> in ask id");
	}
	private void showUnknownVendorError(int brokerId,int vendorId){
		showWarningResult("Vendor " + vendorId + " from " + brokerId +  " is unknown. you should ask its ID from <host>:<port> in ask id");
	}
	
	private void instructionAskID(String host, int port){
		Message message = new Message(0, 0, 0);
		message.setCommand(new MessageCommand(MessageCommand.ASK_ID));
		Message result = Client.sendMessageAndGetResult(message, host, port);
		if(result.getStatus() != 0){
			messageErrorHandler(result);
			return;
		}
		ArrayList<MessageID> ids = result.getRequestedIds();
		if(ids == null){
			showWarningResult("There is no active server on " + host + ":" + port);
			return;
		}
		for(int i = 0; i < ids.size(); i++){
			int b = ids.get(i).getBrokerId(), v = ids.get(i).getVendorId();
			if(v == -1){
				showAcceptedResult("Broker : " + b);
				if(getBrokerKey(b)==null)
					brokerKeys.add(new BrokerKey(b, host, port));
			}
			else {
				showAcceptedResult("Vendor : " + v + " from broker : " + b);
				if(getVendorKey(b, v) == null)
					vendorKeys.add(new VendorKey(b, v, host, port));
			}
		}
	}
	
	private void instructionRegisterToBroker(int brokerId){
		BrokerKey brokerKey = getBrokerKey(brokerId);
		if(brokerKey == null){
			showUnknownBrokerError(brokerId);
			return;
		}
		if(brokerKey.getCustomerId() != -1){
			showWarningResult("You have already registered at " + brokerId);
			return;
		}
		Message message = new Message(brokerId, brokerKey.getCustomerId(), 0);
		message.setCommand(new MessageCommand(MessageCommand.REGISTER_CLIENT));
		Message result = Client.sendMessageAndGetResult(message, brokerKey.getHost(), brokerKey.getPort());
		if(message.getStatus() != 0){
			messageErrorHandler(result);
			return;
		}
		brokerKey.setRegisteredInfo(result.getAskedId(), result.getExchangeKey());
		showAcceptedResult("You have registered with id: " + result.getAskedId());
	}
	
	private void instructionBuyBrokerScrip(int brokerId, int value){
		if(value > 5000){
			showWarningResult("you cannot buy more than 5$ scrip in a transaction");
			return;
		}
		BrokerKey brokerKey = getBrokerKey(brokerId);
		if(brokerKey == null){
			showUnknownBrokerError(brokerId);
			return;
		}
		if(brokerKey.getCustomerId() == -1){
			showWarningResult("You need to register at " + brokerId +" to buy scrip");
			return;
		}
		Message message = new Message(brokerId, brokerKey.getCustomerId(), 0);
		message.setCommand(new MessageCommand(MessageCommand.BUY_BROKER_SCRIP));
		message.setValue(value);
		Message result = Client.sendMessageAndGetResult(message, brokerKey.getHost(), brokerKey.getPort());
		if(message.getStatus() != 0){
			messageErrorHandler(result);
			return;
		}
//		System.err.println("CUSTOMER BUY BROKER SCRIP RESULT DATA:" +  result.readMessageEncryptedData(brokerKey.getKey()));
//		System.err.println("CUSTOMER BUY BROKER SCRIP RESULT DATA:" +  result.readMessageEncryptedData(brokerKey.getKey()).getBrokerScrip());
		Scrip boughtScrip = result.readMessageEncryptedData(brokerKey.getKey()).getBrokerScrip();
		if(boughtScrip == null){
			showWarningResult("Invalid scrip. try again");
			return;
		}
		brokerKey.addScrip(boughtScrip);
		showAcceptedResult("Transaction done");
	}
	
	private int determineVendorScripValue(int rawVal){
		if(rawVal < 20)
			return 20;
		return rawVal;
	}
	
	private ScripKey instructionBuyVendorScrip(BrokerKey brokerKey, VendorKey vendorKey, int value){
		Scrip scrip = brokerKey.getScripWithValue(value);
		if(scrip == null){
			showWarningResult("Insufficent broker scrip");
			return null;
		}
		Message message = new Message(brokerKey.getBrokerId(), brokerKey.getCustomerId(), 0);
		message.setCommand(new MessageCommand(MessageCommand.BUY_VENDOR_SCRIP));
		MessageEncryptedData encryptedData = MessageEncryptedData.factoryBuildBuyVendorScrip(scrip, new MessageRequest("", value));
		message.writeMessageEncryptedData(encryptedData, brokerKey.getKey());
		message.setAskedId(vendorKey.getVendorId());
		Message result = Client.sendMessageAndGetResult(message, brokerKey.getHost(), brokerKey.getPort());
		if(result.getStatus() != 0){
			messageErrorHandler(result);
			return null;
		}
		MessageEncryptedData resultData = result.readMessageEncryptedData(brokerKey.getKey());
		if(resultData == null){
			showWarningResult("Unable to buy vendor scrip");
			return null;
		}
		brokerKey.removeScrip(scrip);
		brokerKey.addScrip(resultData.getBrokerScrip());
		ScripKey boughtScrip = new ScripKey(resultData.getVendorScrip(), resultData.getEncryptedKey(), resultData.getMintId());
		vendorKey.addScrip(boughtScrip);
		showAcceptedResult("you have bought " + boughtScrip.getScrip().getScriptData().getValue() + "S from " + vendorKey.getVendorId() + " m:"+ boughtScrip.getScrip().getVenderId());
		return  boughtScrip;
	}
	
	private void instructionBuyFromVendor(int brokerId, int vendorId, MessageRequest request){
		if( request.getValue() > 5000){
			showWarningResult("you need to buy with macro payment for this transaction");
			return;
		}
		BrokerKey brokerKey = getBrokerKey(brokerId);
		if(brokerKey == null){
			showUnknownBrokerError(brokerId);
			return;
		}
		if(brokerKey.getCustomerId() == -1){
			showWarningResult("You need to register at broker " + brokerId +" to use vendor " + vendorId);
			return;
		}
		VendorKey vendorKey = getVendorKey(brokerId, vendorId);
		if(vendorKey == null){
			showUnknownVendorError(brokerId, vendorId);
			return;
		}
		
		Message message = new Message(vendorId, brokerKey.getCustomerId(), 0);
		message.setCommand(new MessageCommand(MessageCommand.BUY_FROM_VENDOR));
		ScripKey vendorScrip = vendorKey.getScripKeyWithValue(request.getValue());
		if(vendorScrip == null)
			vendorScrip = instructionBuyVendorScrip(brokerKey, vendorKey, determineVendorScripValue(request.getValue()));
		if(vendorScrip == null){
			return;
		}

		message.setMintId(vendorScrip.getMintId());
		MessageEncryptedData encryptedData = MessageEncryptedData.factoryBuildBuyFromVendor(vendorScrip.getScrip(), request, vendorScrip.getMintId());
		message.writeMessageEncryptedData(encryptedData, vendorScrip.getKey());
		Message result = Client.sendMessageAndGetResult(message, vendorKey.getHost(), vendorKey.getPort());
		
		if(result.getStatus() != 0){
			messageErrorHandler(result);
			return;
		}
		MessageEncryptedData resultData = result.readMessageEncryptedData(vendorScrip.getKey());
		if(resultData == null){
			showWarningResult("unable to buy from vendor");
			return ;
		}
		vendorKey.removeScrip(vendorScrip);
		vendorKey.addScrip(new ScripKey(resultData.getVendorScrip(), resultData.getEncryptedKey(), resultData.getMintId()));
		showAcceptedResult("you have bought " + resultData.getMessageRequest().getDescription());
		
	}
	private void showAllScrips(){
		System.out.println("broker scrips:" );
		for(int i = 0; i < brokerKeys.size(); i++){
			ArrayList<Scrip> as = brokerKeys.get(i).getScrips();
			if(as != null){
				for(int j = 0; j < as.size();j++)
					showAcceptedResult("    " +brokerKeys.get(i).getBrokerId() + " : " +as.get(j));
			}
		}
		System.out.println("----------------------------------------" );
		System.out.println("vendor scrips:" );
		for(int i = 0; i < vendorKeys.size(); i++){
			ArrayList<ScripKey> as = vendorKeys.get(i).getScrips();
			if(as != null){
				for(int j = 0; j < as.size();j++)
					showAcceptedResult("    " +vendorKeys.get(i).getBrokerId()+ "$" + vendorKeys.get(i).getVendorId() + " : " +as.get(j).getScrip());
			}
		}
		System.out.println("----------------------------------------" );
	}
	
	private void getInstruction(Scanner scanner){
		System.out.println("chose action");
		System.out.println("[A]: Ask id");
		System.out.println("[R]: Register to broker");
		System.out.println("[M]: Buy scrip from broker by macro payment");
		System.out.println("[B]: Buy from vendor");
		System.out.println("[S]: Show scrips");
		
//		System.out.println("A <host> <port>                           : ask for ID");
//		System.out.println("R <id> <host> <port>                      : register to broker <id>");
//		System.out.println("M <id> <value> <host> <port>              : buy <value> scrip from broker <id> by macropeyment");
//		System.out.println("B <broker-id> <vendor-id> <host> <port>   : buy from vendor <vid> from <bid>");
		
		String ins = scanner.nextLine();
		if(ins.equals(""))
			ins = scanner.nextLine();
		try{
			switch (ins) {
			case "A":
				String host;
				int port;
				System.out.println("please enter host name:");
				host = scanner.next();
				System.out.println("please enter port number:");
				port = scanner.nextInt();
				instructionAskID(host, port);
				break;
			case "R":
				int brokerId;
				System.out.println("please enter broker id:");
				brokerId = scanner.nextInt();
				instructionRegisterToBroker(brokerId);
				break;
			case "M":
				int value;
				System.out.println("please enter broker id:");
				brokerId = scanner.nextInt();
				System.out.println("please enter scrip value [in 0.001$]");
				value = scanner.nextInt();
				instructionBuyBrokerScrip(brokerId, value);
				break;
			case "B":
				int vendorId;
				String desc;
				System.out.println("please enter broker id:");
				brokerId = scanner.nextInt();
				System.out.println("please enter vendor id:");
				vendorId = scanner.nextInt();
				System.out.println("please enter price [in 0.001$]");
				value = scanner.nextInt();
				System.out.println("please enter request:");
				desc = scanner.nextLine();
				desc = scanner.nextLine();
//				System.err.println("nextline done");
				instructionBuyFromVendor(brokerId, vendorId, new MessageRequest(desc, value));
				break;
			case "S":
				showAllScrips();
				break;
				
			default:
				showWarningResult("Invalid instruction");
				break;
			}
		}catch(Exception e){
			showWarningResult("Invalid instruction");
		}
	
	}
	public void run(){
		System.out.println("RUNNING MILLICENT CUSTOMER.");
		Scanner scanner = new Scanner(System.in);
		while(true){
			getInstruction(scanner);
		}
	}
	public Customer() {
		super();
		this.brokerKeys = new ArrayList<Customer.BrokerKey>();
		this.vendorKeys = new ArrayList<Customer.VendorKey>();
	}
	
}