package millicent;

import java.io.Serializable;
import java.util.Date;

public class Scrip implements Serializable{
	private static final long serialVersionUID = 8059544811201056382L;
	public static class ScripData implements Serializable{
		private static final long serialVersionUID = -1238525524591718221L;
		private int scripId, venderId, value, customerId;
		private Date expireDate;
		
		public ScripData(int scripId, int venderId, int value, int customerId,
				Date expireDate) {
			super();
			this.scripId = scripId;
			this.venderId = venderId;
			this.value = value;
			this.customerId = customerId;
			this.expireDate = expireDate;
		}
		
		
		public int getScripId() {
			return scripId;
		}
		public int getVenderId() {
			return venderId;
		}
		public int getValue() {
			return value;
		}
		public int getCustomerId() {
			return customerId;
		}
		public Date getExpireDate() {
			return expireDate;
		}
		
	}
	private ScripData scriptData;
	private byte[] certificate;
	
	
	public Scrip(ScripData scriptData, byte[] certificate) {
		super();
		this.scriptData = scriptData;
		this.certificate = certificate;
	}
	public ScripData getScriptData() {
		return scriptData;
	}
	public byte[] getCertificate() {
		return certificate;
	}
	public int getVenderId(){
		return scriptData.getVenderId();
	}
	public int getCustomerId(){
		return scriptData.getCustomerId();
	}
	@Override
	public String toString() {
		return "scrip[id:" + scriptData.getScripId() + " mintId:" + getVenderId() + " cId:" + getCustomerId() + " value:" + getScriptData().getValue() + "]";
	}
	
}