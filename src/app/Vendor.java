package app;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.Key;
import java.util.ArrayList;
import java.util.Scanner;

import network_data.Message;
import network_data.MessageRequest;
import network_data.Message.MessageEncryptedData;
import network_data.MessageCommand;
import network_data.Message.MessageID;
import millicent.Scrip;
import millicent.ScripMinter;
import connection.Client;
import connection.MessageHandler;
import connection.Server;

public class Vendor implements MessageHandler{
	private ArrayList<ScripMinter> scripMinters;
	private ScripMinter mainMinter;
	private static final int mintScripLimit = 10000;
	private static class BrokerInfo{
		private int brokerId, vendorId;
		private Key key;
		private String host;
		private int port;
		public BrokerInfo(int brokerId, String host, int port) {
			super();
			this.brokerId = brokerId;
			this.host = host;
			this.port = port;
			key = null;
			vendorId = -1;
		}
		public void setRegisteredInfo(int vendorId, Key key) {
			this.key = key;
			this.vendorId = vendorId;
		}
		
		public int getVendorId() {
			return vendorId;
		}
		public Key getKey() {
			return key;
		}
		public int getBrokerId() {
			return brokerId;
		}
		public String getHost() {
			return host;
		}
		public int getPort() {
			return port;
		}
		
	}
	private int vendorServerPort;
	private ArrayList<BrokerInfo> brokerInfos;

	private BrokerInfo getBrokerInfo(int brokerId){
		if(brokerInfos == null)
			return null;
		for(int i = 0; i < brokerInfos.size(); i++)
			if(brokerInfos.get(i).getBrokerId() == brokerId)
				return brokerInfos.get(i);
		return null;
	}
	private BrokerInfo getBrokerInfoByVendorId(int vendorId){
		if(brokerInfos == null)
			return null;
		for(int i = 0; i < brokerInfos.size(); i++)
			if(brokerInfos.get(i).getVendorId() == vendorId)
				return brokerInfos.get(i);
		return null;
	}
	
	private ScripMinter findMinter(int mintId){
		if(scripMinters == null)
			return null;
		for(int i = 0; i < scripMinters.size(); i++)
			if(scripMinters.get(i).getMinterId() == mintId)
				return scripMinters.get(i);
		return null;
	}
	
	private Scrip mintchangeScrip(int value, int custumerId){
		return mainMinter.mintScrip(value, custumerId);
	}
	
	public Vendor() {
		mainMinter = ScripMinter.getInstance(10*mintScripLimit);
		mainMinter.generateConsumedRange();
		brokerInfos = new ArrayList<Vendor.BrokerInfo>();
		scripMinters = new ArrayList<ScripMinter>();
		scripMinters.add(mainMinter);
	}
	

	private void messageErrorHandler(Message error){
		if(error.getError() != null)
			System.out.println("ERROR: " + error.getError() + ".");
		else
			System.out.println("UNKNOWN ERROR: INSTRUCTION TERMINATED.");
	}
	private void showAcceptedResult(String s){
		System.out.println(s + ".");
	}
	private void showWarningResult(String s){
		System.out.println(s + ".");
	}
	private void showUnknownBrokerError(int brokerId){
		showWarningResult("Broker " + brokerId +  " is unknown. you should ask its ID from <host>:<port> in ask id");
	}

	private void instructionAskID(String host, int port){
		Message message = new Message(0, 0, 0);
		message.setCommand(new MessageCommand(MessageCommand.ASK_ID));
		Message result = Client.sendMessageAndGetResult(message, host, port);
		if(result.getStatus() != 0){
			messageErrorHandler(result);
			return;
		}
		ArrayList<MessageID> ids = result.getRequestedIds();
		if(ids == null){
			showWarningResult("There is no active broker on " + host + ":" + port);
			return;
		}
		for(int i = 0; i < ids.size(); i++){
			int b = ids.get(i).getBrokerId(), v = ids.get(i).getVendorId();
			if(v == -1){
				showAcceptedResult("Broker : " + b);
				if(getBrokerInfo(b)==null)
					brokerInfos.add(new BrokerInfo(b, host, port));
			}
		}
	}
	
	private void instructionRegisterToBroker(int brokerId){
		BrokerInfo brokerInfo = getBrokerInfo(brokerId);
		if(brokerInfo == null){
			showUnknownBrokerError(brokerId);
			return;
		}
		if(brokerInfo.getVendorId() != -1){
			showWarningResult("You have already registered at " + brokerId);
			return;
		}
		Message message = new Message(0, 0, 0);
		message.setCommand(new MessageCommand(MessageCommand.REGISTER_VENDOR));
		message.setValue(vendorServerPort);
		Message result = Client.sendMessageAndGetResult(message, brokerInfo.getHost(), brokerInfo.getPort());
		if(message.getStatus() != 0){
			messageErrorHandler(result);
			return;
		}
		brokerInfo.setRegisteredInfo(result.getAskedId(), result.getExchangeKey());
		showAcceptedResult("You have registered with id: " + result.getAskedId());
	}
	

	private void getInstruction(Scanner scanner){
		System.out.println("chose action");
		System.out.println("[A]: Ask id");
		System.out.println("[R]: Register to broker");
		
		String ins = scanner.next();
		try{
			switch (ins) {
			case "A":
				String host;
				int port;
				System.out.println("please enter host name:");
				host = scanner.next();
				System.out.println("please enter port number:");
				port = scanner.nextInt();
				instructionAskID(host, port);
				break;
			case "R":
				int brokerId;
				System.out.println("please enter broker id:");
				brokerId = scanner.nextInt();
				instructionRegisterToBroker(brokerId);
				break;
				
			default:
				showWarningResult("Invalid instruction");
				break;
			}
		}catch(Exception e){
			showWarningResult("Invalid instruction");
		}
	}

	private void respondAskId(Socket socket, Message message){
		Message respond = new Message(0, 0, 0);
		ArrayList<MessageID> ids = new ArrayList<Message.MessageID>();
		for(int i = 0; i < brokerInfos.size(); i++)
			if(brokerInfos.get(i).getVendorId() != -1)
				ids.add(new MessageID(brokerInfos.get(i).getBrokerId(), brokerInfos.get(i).getVendorId()));
		respond.setRequestedIds(ids);
		Client.sendMessageReply(respond, socket);
	}
	private void responseBuyFromVendor(Socket socket, Message message){
		BrokerInfo brokerInfo = getBrokerInfoByVendorId(message.getVendorId());
		if(brokerInfo == null){
			showWarningResult("Invalid vendor Id. message terminated");
			return;
		}
		ScripMinter mint = findMinter(message.getMintId());
		if(mint == null){
			showWarningResult("Invalid minter. message terminated");
			return;
		}

		Message respond = null;
		Key key = mint.getCustumerKey(message.getCustumerId());
		MessageEncryptedData encryptedData = message.readMessageEncryptedData(key);
		int remainingMoney = encryptedData.getVendorScrip().getScriptData().getValue() - encryptedData.getMessageRequest().getValue();
		if(remainingMoney < 0){
			showWarningResult("insufficent money. send unautherized message");
			respond =  new Message(message.getVendorId(), message.getCustumerId(), 2);
			respond.setError("insufficent money");
			Client.sendMessageReply(respond, socket);
			return;
			
		}
		boolean paymentDone = mint.consumeScripIfValid(encryptedData.getVendorScrip());
		if(paymentDone == false){
			showWarningResult("Invalid scrip. send unautherized message");
			respond =  new Message(message.getVendorId(), message.getCustumerId(), 2);
			respond.setError("Invalid payment");
			Client.sendMessageReply(respond, socket);
			return;
		}
		respond =  new Message(message.getVendorId(), message.getCustumerId(), 0);
		MessageRequest responseReq = new MessageRequest(message.getVendorId() + "'s Info" , encryptedData.getMessageRequest().getValue());
		MessageEncryptedData respondData = MessageEncryptedData.factoryBuildVendorSell(mainMinter.getCustumerKey(message.getCustumerId()), mintchangeScrip(remainingMoney, message.getCustumerId()), responseReq, mainMinter.getMinterId());
		respond.writeMessageEncryptedData(respondData, key);
		Client.sendMessageReply(respond, socket);
		showAcceptedResult("Sold "+encryptedData.getMessageRequest().getValue() + "S info");
	}
	
	private void responseToMinterRequest(Socket socket, Message message){
		BrokerInfo brokerInfo = getBrokerInfo(message.getVendorId());
		if(brokerInfo == null || brokerInfo.getVendorId() != message.getCustumerId()){
			showWarningResult("Invalid minter request");
			return;
		}
		
		ScripMinter minter = ScripMinter.getInstance(mintScripLimit);
		Message respond = new Message(message.getVendorId(), message.getCustumerId(), 0);
		MessageEncryptedData encryptedData = MessageEncryptedData.factoryBuilRequestScripMinter(minter);
		respond.writeMessageEncryptedData(encryptedData, brokerInfo.getKey());
		Client.sendMessageReply(respond, socket);
		showAcceptedResult("Send a minter to broker " + message.getVendorId());
		minter.generateConsumedRange();
		scripMinters.add(minter);
	}
	
	@Override
	public void handleMessage(Socket socket) {
		Message message = null;
		try{
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			Object obj = ois.readObject();
			if(obj instanceof Message)
				message = (Message)obj;
			else{
				showWarningResult("Invalid message");
			}
			MessageCommand command = message.getCommand();
			if(command.getCommand().equals(MessageCommand.ASK_ID)){
				respondAskId(socket, message);
			}
			else if(command.getCommand().equals(MessageCommand.BUY_FROM_VENDOR)){
				responseBuyFromVendor(socket, message);
			}
			else if(command.getCommand().equals(MessageCommand.REQ_VENDOR_MINT)){
				responseToMinterRequest(socket, message);
			}
			else {
				showWarningResult("Invalid message command. message terminated");
			}
		}catch(Exception e){
			showWarningResult("Invalid connection terminated");
		}finally{
			try {
				socket.close();
			} catch (IOException e) {
			}
		}
		
	}
	public void run(){
		Scanner scanner = null;
		try {
			System.out.println("RUNNING MILLICENT VENDOR.");
			scanner = new Scanner(System.in);
			System.out.println("please enter port number");
			vendorServerPort = scanner.nextInt();
			Server server;
			server = new Server(new ServerSocket(vendorServerPort, 6), this);
			server.start();
		} catch (IOException e) {
			System.out.println("unable to run server.");
			System.out.println("program terminated.");
		}
		while(true){
			getInstruction(scanner);
		}
	}
}
