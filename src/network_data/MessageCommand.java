package network_data;

import java.io.Serializable;

public class MessageCommand implements Serializable{
	private static final long serialVersionUID = -2967956490766982578L;

	public static String ASK_ID = "ASK_ID";
	public static String REGISTER_CLIENT = "REGISTER_CLIENT";
	public static String REGISTER_VENDOR = "REGISTER_VENDOR";
	public static String BUY_BROKER_SCRIP = "BUY_BROKER_SCRIP";
	public static String BUY_VENDOR_SCRIP = "BUY_VENDOR_SCRIP";
	public static String BUY_FROM_VENDOR = "BUY_FROM_VENDOR";
	public static String REQ_VENDOR_MINT = "REQ_VENDOR_MINT";
	
	private String command;

	public String getCommand() {
		return command;
	}
	public MessageCommand(String command) {
		super();
		this.command = command;
	}
	@Override
	public String toString() {
		return command;
	}
}
