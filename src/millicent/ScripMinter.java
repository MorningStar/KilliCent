package millicent;

import java.io.Serializable;
import java.security.Key;
import java.security.SecureRandom;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Date;

import security.SecUtil;
import millicent.Scrip.ScripData;

public class ScripMinter implements Serializable{
	private static final long serialVersionUID = -1196084633041587274L;
	private int minterId;
	private int scripIdOffset, scripNumLimit, currentScripId;
	private Date expireDate;
	
	private MasterSecretRing venderMasterSecrets, customerMasterSecrets;
	private BitSet isConsumed;
	
	
	
	private ScripMinter(int minterId, int scripIdOffset, int scripNumLimit,
			Date expireDate) {
		super();
		this.minterId = minterId;
		this.scripIdOffset = scripIdOffset;
		currentScripId = scripIdOffset;
		this.scripNumLimit = scripNumLimit;
		this.expireDate = expireDate;
		
		isConsumed = null;
		venderMasterSecrets = MasterSecretRing.getNewInstance(6);
		customerMasterSecrets = MasterSecretRing.getNewInstance(6);
	}
	public static ScripMinter getInstance(int scripNumLimit){
		SecureRandom rnd = new SecureRandom();
		Calendar newxtMnt = Calendar.getInstance();
		newxtMnt.add(Calendar.MONTH, 1);
		return new ScripMinter(rnd.nextInt(), rnd.nextInt(100000000), scripNumLimit, newxtMnt.getTime());
	}
	
	public int getMinterId() {
		return minterId;
	}
	private synchronized int getNewScripId(){
		if(canMint() == false)
			return -1;
		return currentScripId++;
	}
	
	public void generateConsumedRange(){
		isConsumed = new BitSet(scripNumLimit);
	}
	private boolean isExpired(Date d){
		return d.after(expireDate);
	}
	public boolean canMint(){
		if(currentScripId >= scripIdOffset+scripNumLimit)
			return false;
		if(isExpired(Calendar.getInstance().getTime()))
			return false;
		return true;
	}
	
	public Scrip mintScrip(int value, int customerId){
		if(value == 0)
			return null;
		int scrupId = getNewScripId();
		if(scrupId == -1)
			return null;
		ScripData scripData = new ScripData(scrupId, minterId, value, customerId, expireDate);
		return new Scrip(scripData, generatedScripCertificate(scripData));
	}
	
	private byte[] generatedScripCertificate(ScripData scripData){
		byte[] data = SecUtil.appendByteArrays(SecUtil.convertObjectToByteArray(scripData), venderMasterSecrets.getSecret(scripData.getScripId()));
		return SecUtil.generateMD5Hash(data);
	}
	
	public boolean isCertificateValid(Scrip scrip){
		return SecUtil.isByteArraysEqual(scrip.getCertificate(), generatedScripCertificate(scrip.getScriptData()));
	}
	
	public Key getCustumerKey(int customerId){
		Key key = SecUtil.buildSecretKeyByMD5(SecUtil.appendByteArrays(SecUtil.intToByteArray(customerId), customerMasterSecrets.getSecret(customerId)));
//		System.err.println("get customer:" + customerId + " key:" + key);
		return key;
	}
	
	public boolean isScripValid(Scrip scrip){
//		System.err.println("check scrip:" + scrip + " validity at minter:" + minterId);
		if(isExpired(scrip.getScriptData().getExpireDate()))
			return false;
		if(scrip.getScriptData().getScripId() < scripIdOffset || scrip.getScriptData().getScripId() >= scripIdOffset+scripNumLimit)
			return false;
		if(isConsumed == null || isConsumed.get(scrip.getScriptData().getScripId() - scripIdOffset))
			return false;
		if(isCertificateValid(scrip) == false)
			return false;
		return true;
	}
	private void consumeScrip(Scrip scrip){
		try{
			isConsumed.set(scrip.getScriptData().getScripId()-scripIdOffset);
		}catch(Exception e){
		}
		
	}
	public synchronized boolean consumeScripIfValid(Scrip scrip){
		boolean out = isScripValid(scrip);
		if(out)
			consumeScrip(scrip);
		return out;
	}
}
