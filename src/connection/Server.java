package connection;

import java.io.IOException;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread implements Serializable{
	private static final long serialVersionUID = -5074861182651646123L;
	private ServerSocket serverSocket;
	private MessageHandler handler;
	
	public Server(ServerSocket serverSocket, MessageHandler handler) {
		super();
		this.serverSocket = serverSocket;
		this.handler = handler;
	}
	
	@Override
	public void run() {
		System.err.println("START RUNNING SERVER.");
		while(true){
			Socket socket;
			try {
				socket = serverSocket.accept();
				handler.handleMessage(socket);
//				System.err.println("SERVER ACEPT END.");
			} catch (IOException e) {
			}
			
		}
	}
}
