package connection;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;

import network_data.Message;

public class Client {
	private static int soTimeout = 5000;
	public static Message sendMessageAndGetResult(Message message, String host, int port){
		Message result = null;
		Socket socket = null;
		try{
			System.err.println("send message :" +message);
			socket = new Socket(host, port);
			socket.setSoTimeout(soTimeout);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
//			System.err.println("WRITE Start");
			oos.writeObject(message);
			oos.flush();
//			System.err.println("WRITE END");
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
//			System.err.println("read start");
			Object obj = ois.readObject();
			System.err.println("get response:" + obj);
			if(obj instanceof Message)
				result = (Message) obj;
		}catch(SocketTimeoutException e){
			result = new Message(0, 0, Message.ERROR_SOKET_TIMEOUT_EXP);
			result.setError("Connection time out");
			return result;
		}catch(Exception e){
			System.err.println("ERROR :" + e.getMessage() );
//			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
			}
		}
		return result;
	}
	public static void sendMessageReply(Message message, Socket socket){
		try{
			System.err.println("CLIENT SEND MESSAGE :" + message);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
//			System.err.println("START WRITE:" + message);
			oos.writeObject(message);
			oos.flush();
//			System.err.println("WRITE END");
			
		}catch(Exception e){
			System.err.println("ERROR" + e.getMessage());
//			e.printStackTrace();
		}finally{
			try {
				socket.close();
			} catch (Exception e) {
			}
		}
	}
}
