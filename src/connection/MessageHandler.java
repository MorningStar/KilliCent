package connection;

import java.net.Socket;

public interface MessageHandler {
	public void handleMessage(Socket socket);
}
