package millicent;

import java.io.Serializable;
import java.security.SecureRandom;

public class MasterSecretRing implements Serializable{
	private static final long serialVersionUID = -8197413918066684807L;
	private byte[] secret[];
	private int secrentNum;
	
	private MasterSecretRing(byte[] b[], int secrentNum){
		secret = b;
		this.secrentNum = secrentNum;
	}
	public static MasterSecretRing getNewInstance(int num){
		if(num < 1)
			num = 1;
		byte[] sec[] = new byte[num][];
		SecureRandom secRandom = new SecureRandom();
		for(int i = 0; i < num; i++){
			sec[i] = new byte[16];
			secRandom.nextBytes(sec[i]);
		}
		return new MasterSecretRing( sec , num);
	}
	public byte[] getSecret(int id) {
		if(id < 0)
			id *= -1;
		return secret[id%secrentNum];
	}
}
